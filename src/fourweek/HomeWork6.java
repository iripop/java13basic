package fourweek;

import java.util.Scanner;

//TODO IRINA
public class HomeWork6 {

    public static final String[] morse = new String[]{".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....", "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        if(s.length() < 1000) {
            for (int i = 0; i < s.length(); i++) {
                if (i > 0 ){
                    System.out.print(" ");
                }
                System.out.print(morse[(int)s.charAt(i) - 1040]);
            }
            System.out.println();
        }
    }
}
