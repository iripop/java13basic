package fourweek;

import java.util.Scanner;

//TODO IRINA
public class HomeWork5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 0 && n < 100) {
            int[] ai = new int[n];

            for (int i = 0; i < n; i++) {

                int temp = scanner.nextInt();
                if (temp > -1000 && temp < 1000) {
                    ai[i] = temp;
                }
            }

            int m = scanner.nextInt();
            if (m > 0 && m < 100) {
                for (int i = 0; i < ai.length; i++) {
                    if (i > 0) {
                        System.out.print(" ");
                    }
                    System.out.print(ai[(i + m + 1) % ai.length]);
                }
                System.out.println();
            }
        }
    }
}
