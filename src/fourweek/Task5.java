package fourweek;

import java.util.Scanner;
/*

/*
Дано целое число n, n > 0. Вывести сумму всех цифр этого числа.
92180 -> 20 //9 + 2 + 1 + 8 + 0 == 20
 */
public class Task5 {
    public static void main(String[] args) {

            Scanner scanner =   new Scanner(System.in);
            int n = scanner.nextInt();
            int s = 0;

            while (n > 0){
                s += n%10;
                n /= 10;
                System.out.println(n);
                System.out.println(s);

            }
        }
    }


