package fourweek;

import java.util.Scanner;

//TODO IRINA
public class HomeWork4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 0 && n < 1000) {
            int[] ai = new int[n];

            for (int i = 0; i < n; i++) {

                int temp = scanner.nextInt();
                if (temp > -1000 && temp < 1000) {
                    ai[i] = temp;
                }
            }
            printDifferences(ai);
        }
    }

    private static void printDifferences(int[] a) {
        int count = 1;
        for (int i = 1; i < a.length; i++) {
            if (a[i - 1] != a[i]) {
                System.out.println("" + count + " " + a[i - 1]);
                count = 1;
            } else {
                count++;
            }
        }
        System.out.println("" + count + " " + a[a.length - 1]);
    }
}
