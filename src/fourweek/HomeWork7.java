package fourweek;

import java.util.Scanner;

//TODO IRINA
public class HomeWork7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 0 && n < 1000) {
            int[] ai = new int[n];

            for (int i = 0; i < n; i++) {

                int temp = scanner.nextInt();
                if (temp > -1000 && temp < 1000) {
                    ai[i] = temp;
                }
            }
            int[] result = makePoweredArray(ai);
            for (int i = 0; i < result.length; i++) {
                if (i > 0) {
                    System.out.print(" ");
                }
                System.out.print(result[i]);
            }
            System.out.println();
        }
    }

    private static int[] makePoweredArray(int[] a) {
        int left = 0;
        int right = a.length - 1;
        int pos = a.length - 1;
        int[] res = new int[a.length];
        for (int i = a.length - 1; i >= 0 ; i--) {
            if(Math.abs(a[left]) > Math.abs(a[right])) {
                res[i] = a[left] * a[left];
                left++;
            } else {
                res[i] = a[right] * a[right];
                right--;
            }

        }
        return res;
    }
}
