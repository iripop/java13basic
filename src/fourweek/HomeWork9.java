package fourweek;

import java.util.Scanner;

//TODO IRINA
public class HomeWork9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 0 && n < 100) {
            String[] ai = new String[n];
            String duplicate = null;

            for (int i = 0; i < n; i++) {
                String temp = scanner.next();
                ai[i] = temp;
                if (duplicate == null) {
                    for (int j = 0; j < i; j++) {
                        if (ai[j].equals(temp)) {
                            duplicate = temp;
                        }
                    }
                }
            }
            System.out.println(duplicate);
        }
    }
}
