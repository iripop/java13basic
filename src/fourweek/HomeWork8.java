package fourweek;

import java.util.Scanner;

//TODO IRINA
public class HomeWork8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        if (n > 0 && n < 1000) {
            int[] ai = new int[n];

            for (int i = 0; i < n; i++) {

                int temp = scanner.nextInt();
                if (temp > -1000 && temp < 1000) {
                    ai[i] = temp;
                }
            }
            // проверить n


            // проверить введенное число и заполнить ai
            int m = scanner.nextInt();

            if (m > -1000 && m < 1000) {
                // проверить m
                int result = findClosest(ai, m);
                System.out.println(result);
            }
        }
    }

    private static int findClosest(int[] a, int m) {
        int result = a[0];
        int minDiff = Math.abs(result - m);
        for (int i = 1; i < a.length; i++) {
            if (Math.abs(a[i] - m) <= minDiff) {
                result = a[i];
                minDiff = Math.abs(result - m);
            }
        }
        return result;
    }
}
