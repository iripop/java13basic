package week7.oop1.task1;

public class Main {
    public static void main(String[] args) {
        Bulb bulb = new Bulb();
        System.out.println("Светит ли лампа сейчас? - " + bulb.isShining());
        bulb.turnOff();
        System.out.println("Светит ли лампа сейчас? - " + bulb.isShining());

        Chandelier chandelier = new Chandelier(4);
        chandelier.turnOn();
        chandelier.turnOff();
    }
}
