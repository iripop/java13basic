package week7.oop1.task2;

public class Thermometer {
    private double tempFahrenheit;
    private double tempCelsius;

    public Thermometer(double currentTemperature, TemperatureUnit temperatureUnit) {

        if (temperatureUnit == TemperatureUnit.CELSIUS) {
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
        } else if (temperatureUnit == TemperatureUnit.FAHRENHEIT) {
            tempCelsius = fromFahrenheitToCelsius(currentTemperature);
            tempFahrenheit = currentTemperature;
        } else {
            System.out.println("Температура не распознана. Единица измерения по умолчанию = цельсий");
            tempCelsius = currentTemperature;
            tempFahrenheit = fromCelsiusToFahrenheit(currentTemperature);
            //System.out.println("ERROR! ALARM!");
            //throw new UnsupportedOperationException("Температура не распознана");
        }
    }

    public double getTempCelsius() {
        return tempCelsius;
    }

    public double getTempFahrenheit() {
        return tempFahrenheit;
    }


    private double fromCelsiusToFahrenheit(double currentTemperature) {
        return currentTemperature * 1.8 + 32;
    }

    private double fromFahrenheitToCelsius(double currentTemperature) {
        return (currentTemperature - 32) / 1.8;
    }

    public enum TemperatureUnit {
        CELSIUS,
        FAHRENHEIT
    }

    public static void main(String[] args) {
        //Thermometer thermometer = new Thermometer(-25, "F");
        Thermometer thermometer = new Thermometer(-25, TemperatureUnit.FAHRENHEIT);
        Thermometer thermometer1 = new Thermometer(-25, TemperatureUnit.CELSIUS);
        System.out.println("В цельсиях: " + thermometer.getTempCelsius());
        System.out.println("В Фаренгейтах: " + thermometer.getTempFahrenheit());

        System.out.println("В цельсиях: " + thermometer1.getTempCelsius());
        System.out.println("В Фаренгейтах: " + thermometer1.getTempFahrenheit());
    }
}

/*Имя
    Должно содержать только буквы. Начинаться с заглавной буквы и далее только прописные.
    От 2 до 20 символов.
День рождения
    Должно иметь вид DD.MM.YYYY (DD, MM, YYYY - цифры, без ограничений)
Номер телефона
    Должно начинаться со знака +, далее ровно 11 цифр.
Email
    В начале идут прописные буквы или цифры или один из спец. символов _ - * .
    Далее обязательно символ @
    Далее прописные буквы или цифры
    Далее точка
    Далее “com” или “ru”
*/

