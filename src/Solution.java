import java.util.Scanner;

public class Solution {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 0 && n < 1000000) {
            print(n);
        }
    }

    private static void print(int n) {
        if (n <= 0) {
            return;
        }
        if (n / 10 > 0) {
            print(n / 10);
            System.out.print(" ");
        }
        System.out.print(n % 10);
    }
}



