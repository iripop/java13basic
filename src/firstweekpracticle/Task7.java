package firstweekpracticle;

import java.util.Scanner;

public class Task7 {

    public static final double LITRES_PER_GALLON = 0.219969;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double res = n * LITRES_PER_GALLON;
        System.out.println("Результат в галлонах " + res);
    }
}