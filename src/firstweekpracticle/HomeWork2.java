package firstweekpracticle;

import java.util.Scanner;

public class HomeWork2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        if (a>0 && b<100) {
            System.out.println("Среднее квадратическое: " + Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 2));
        } else {
            System.out.println("Некорректное значение");
        }
    }
}
