package firstweekpracticle;

import java.util.Scanner;

public class HomeWork7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        if (a > 9 && a < 100) {
            int start = a / 10;
            int end = a % 10;
            System.out.println("" +  end + start);
        } else {
            System.out.println("Некорректное число");
        }
    }

}
