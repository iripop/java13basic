package secondweekpractice;

import java.util.Scanner;

/*
Реализовать System.out.println(), используя System.out.print() и переноса на новую строку \n
Входные данные: два слова, считываемые из консоли

Входные данные
Hello World
Выходные данные
Hello
World

 */

public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.next();
        String b = scanner.next();
        System.out.print(a + "\n");
        System.out.print(b + "\n");
    }
}
