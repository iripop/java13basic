package secondweekpractice;

import java.util.Scanner;
/*
Считать данные из консоли о типе номера отеля.
        1 VIP, 2 Premium, 3 Standard
        Вывести цену номера VIP = 125, Premium = 110, Standard = 100
 */

public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int t = scanner.nextInt();
        /*
        if (t > 0 && t < 4) {
            if (t == 1) {
                System.out.println("номер VIP, цена 125");
            } else if (t == 2) {
                System.out.println("номер Premium, цена 110");

            } else if (t == 3) {
                System.out.println("номер Standard, цена 100");

            }
        } else {
            System.out.println("Неверный тип номера");
        }*/
        switch (t) {
            case 1:
                System.out.println("номер VIP, цена 125");
                break;
            case 2:
                System.out.println("номер Premium, цена 110");
                break;
            case 3:
                System.out.println("номер Standard, цена 100");
                break;
            default:
                System.out.println("введите корректный номеп");

        }

        switch (t) {
            case 1 -> System.out.println("номер VIP, цена 125");
            case 2 -> System.out.println("номер Premium, цена 110");
            case 3 -> System.out.println("номер Standard, цена 100");
            default -> System.out.println("введите корректный номеп");
        }
    }
}
