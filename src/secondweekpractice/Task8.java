package secondweekpractice;

import java.util.Scanner;

/*
Дан символ, поменять со строчного на заглавный или с заглавного на строчный

Входные данные
d
Выходные данные
D
Входные данные
A
Выходные данные
a
 */

public class Task8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String c = scanner.next();
        char c1 = c.charAt(0);
        if (c1 >= 'a' && c1 <= 'z') {
            System.out.println((char)(c1 + 'A' - 'a'));
        } else {
            System.out.println((char)(c1 - ('A' - 'a')));
        }
        /*
        if (c.toUpperCase().equals(c)) {
            System.out.println(c.toLowerCase());
        } else {
            System.out.println(c.toUpperCase());
        }
         */
    }
}
