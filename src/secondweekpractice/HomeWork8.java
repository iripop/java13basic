package secondweekpractice;

import java.util.Scanner;

public class HomeWork8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String a = scanner.nextLine();
        int spaceIndex = a.lastIndexOf(" ");
        if (a.length() > 2 && a.length() < 100 && spaceIndex > 0 && spaceIndex != a.length() - 1) {
            System.out.println(a.substring(0, spaceIndex));
            System.out.println(a.substring(spaceIndex + 1));
        }
    }
}
