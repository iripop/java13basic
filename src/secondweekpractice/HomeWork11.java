package secondweekpractice;

import java.util.Scanner;

public class HomeWork11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        if (a > 0 && a < 100 && b > 0 && b < 100 && c > 0 && c < 100) {
            if (a < b + c && b < a + c && c < b + a) {
                System.out.println("true");
            } else {
                System.out.println("false");
            }
        }
    }
}
