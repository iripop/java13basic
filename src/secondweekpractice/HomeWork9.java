package secondweekpractice;

import java.util.Scanner;

public class HomeWork9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        if (a > -1000 && a < 1000) {
            double result = Math.pow(Math.sin(a), 2) + Math.pow(Math.cos(a), 2) - 1;
            result = 0;
            System.out.println(result == 0);
        }
    }
}
