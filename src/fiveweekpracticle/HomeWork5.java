package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // что мы делаем символом ==
        if (n > 0 && n < 100) {
            int[][] a = new int[n][n];
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < n; i++) {
                    int ai = scanner.nextInt();
                    if (ai > 0 && ai < 1000) {
                        a[j][i] = ai;
                    }
                }
            }
            boolean result = isSimmetric(a);
            System.out.println(result);
        }
    }

    private static boolean isSimmetric(int[][] a) {
        for (int j = 0; j < a.length; j++) {
            for (int i = a[j].length - 1; i >= 0; i--) {
                if (a[i][j] != a[a.length - j - 1][a.length - i - 1]) {
                    return false;
                }
            }
        }
        return true;
    }
}