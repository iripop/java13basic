package fiveweekpracticle;

/*
        На вход подается число N - длина массива.
        Затем подается массив целых чисел из N элементов.

        Нужно циклически сдвинуть элементы на 1 влево.

        Входные данные:
        5
        1 2 3 4 7
        Выходные данные:
        2 3 4 7 1
        */


import java.util.Scanner;

public class Task6{
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }



    }
}
