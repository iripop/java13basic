package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork10 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 0 && n < 1000000) {
            print(n);
        }
    }

    private static void print(int n) {
        if (n <= 0) {
            return;
        }
        System.out.print(n % 10);
        if (n / 10 > 0) {
            System.out.print(" ");
            print(n / 10);
        }
    }
}