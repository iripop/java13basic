package fiveweekpracticle;

/*
На вход подается число N — длина массива.
Затем передается массив целых чисел длины N.

Проверить, является ли он отсортированным массивом строго по убыванию.
Если да, вывести true, иначе вывести false.

5
5 4 3 2 1
->
true

2
43 46
->
false

3
5 5 5
->
false

 */


import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        boolean flag = true;
        for (int i = 1; i < n; i++) {
            if (arr[i - 1] < arr[i]) {
                //System.out.println("имент" + arr[i]);
                flag = false;
            }
        }
        if (flag) {
            System.out.println("по убыванию");

        } else {
            System.out.println("не по убыванию");
        }
    }
}
