package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 0 && n < 1000000) {
            System.out.println(sum(n));
        }
    }

    private static int sum(int n) {
        if (n <= 0) {
            return 0;
        }
        return n % 10 + sum(n / 10);
    }
}