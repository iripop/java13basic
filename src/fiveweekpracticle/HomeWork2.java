package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // что мы делаем символом ==
        if (n > 0 && n < 100) {
            int[][] angles = new int[2][2];
            for (int i = 0; i < 2; i++) {
                for (int j = 0; j < 2; j++) {
                    int temp = scanner.nextInt();
                    if (temp >= 0 && temp < n) {
                        angles[i][j] = temp;
                    }
                }
            }
            //printArray(angles);
            int[][] a = new int[n][n];
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < n; i++) {
                    a[j][i] = isBorder(angles, i, j) ? 1 : 0;
                }
            }
            printArray(a);
        }
    }

    private static void printArray(int[][] a) {
        for (int j = 0; j < a.length; j++) {
            for (int i = 0; i < a[j].length; i++) {
                if (i > 0) {
                    System.out.print(" ");
                }
                System.out.print(a[j][i]);
            }
            System.out.println();
        }
    }

    private static boolean isBorder(int[][] angles, int x, int y) {
        for (int i = 0; i < angles.length; i++) {
            if (angles[i][0] == x) {
                int y1 = angles[0][1];
                int y2 = angles[1][1];
                if (y <= Math.max(y1, y2) && y >= Math.min(y1, y2)) {
                    return true;
                }
            }
            if (angles[i][1] == y) {
                int x1 = angles[0][0];
                int x2 = angles[1][0];
                if (x <= Math.max(x1, x2) && x >= Math.min(x1, x2)) {
                    return true;
                }
            }
        }
        return false;
    }
}