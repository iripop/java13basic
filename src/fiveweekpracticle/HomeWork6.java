package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();
        int k = scanner.nextInt();
        int[][] stat = new int[7][5];
        if (checkNuticion(a) && checkNuticion(b) && checkNuticion(c) && checkKalory(k)) {
            for (int i = 0; i < 7; i++) {
                int ai = scanner.nextInt();
                int bi = scanner.nextInt();
                int ci = scanner.nextInt();
                int ki = scanner.nextInt();
                if (checkNuticion(ai) && checkNuticion(bi) && checkNuticion(ci) && checkKalory(ki)) {
                    a -= ai;
                    b -= bi;
                    c -= ci;
                    k -= ki;
                }
            }
            if (a > 0 && b > 0 && c > 0 && k > 0) {
                System.out.println("Отлично");
            } else {
                System.out.println("Нужно есть поменьше");
            }
        }

    }

    private static boolean checkNuticion(int n) {
        return n > 0 && n < 2000;
    }

    private static boolean checkKalory(int k) {
        return k > 0 && k < 20000;
    }
}