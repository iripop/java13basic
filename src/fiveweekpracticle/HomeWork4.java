package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // что мы делаем символом ==
        if (n > 0 && n < 100) {
            int[][] a = new int[n][n];
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < n; i++) {
                    int ai = scanner.nextInt();
                    if (ai > 0 && ai < 1000) {
                        a[j][i] = ai;
                    }
                }
            }
            int p = scanner.nextInt();
            int cutI = 0;
            int cutJ = 0;
            for (int j = 1; j < n; j++) {
                for (int i = 1; i < n; i++) {
                    if (a[i][j] == p) {
                        cutI = i;
                        cutJ = j;
                    }
                }

            }
            int[][] res = new int[n - 1][n - 1];
            for (int jRes = 0, j = 0; jRes < n - 1; jRes++, j++) {
                if(cutJ == j) {
                    j++;
                }
                for (int iRes = 0, i = 0; iRes < n - 1; iRes++, i++) {
                    if(cutI == i) {
                        i++;
                    }
                    res[iRes][jRes] = a[i][j];
                }
            }
            printArray(res);
        }
    }

    private static void printArray(int[][] a) {
        for (int j = 0; j < a.length; j++) {
            for (int i = 0; i < a[j].length; i++) {
                if (i > 0) {
                    System.out.print(" ");
                }
                System.out.print(a[j][i]);
            }
            System.out.println();
        }
    }
}