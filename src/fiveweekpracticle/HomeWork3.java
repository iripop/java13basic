package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // что мы делаем символом ==
        if (n > 0 && n < 100) {
            int[] knight = new int[2];
            for (int i = 0; i < 2; i++) {
                int temp = scanner.nextInt();
                if (temp >= 0 && temp < n) {
                    knight[i] = temp;
                }
            }
            //printArray(knight);
            String[][] a = new String[n][n];
            for (int j = 0; j < n; j++) {
                for (int i = 0; i < n; i++) {
                    a[j][i] = isKnight(knight, i, j) ? "K" : isMove(knight, i, j) ? "X" : "0";
                }
            }
            printArray(a);
        }
    }

    private static void printArray(Object[][] a) {
        for (int j = 0; j < a.length; j++) {
            for (int i = 0; i < a[j].length; i++) {
                if (i > 0) {
                    System.out.print(" ");
                }
                System.out.print(a[j][i]);
            }
            System.out.println();
        }
    }

    private static boolean isKnight(int[] knight, int x, int y) {
        return (knight[0] == x && knight[1] == y);
    }

    private static boolean isMove(int[] knight, int x, int y) {
        if (
                (Math.abs(knight[0] - x) == 2 && Math.abs(knight[1] - y) == 1)
                        ||
                        (Math.abs(knight[0] - x) == 1 && Math.abs(knight[1] - y) == 2)
        ) {
            return true;
        } else {
            return false;
        }
    }
}