package fiveweekpracticle;

/*
   На вход подается два отсортированных массива.
   Нужно создать отсортированный третий массив,
   состоящий из элементов первых двух.

   Входные данные:
   5
   1 2 3 4 7

   2
   1 6

   Выходные данные:
   1 1 2 3 4 6 7


   public static void mergeTwoArrays(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        int i = 0, j = 0, k = 0;

        //обход двух массивов
        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] < arr2[j]) {
                mergedArray[k++] = arr1[i++];
            }
            else {
                mergedArray[k++] = arr2[j++];
            }

            //сохраняем оставшиеся элементы первого массива
            while (i < arr1.length) {
                mergedArray[k++] = arr1[i++];
            }

            //сохраняем оставшиеся элементы второго массива
            while (j < arr2.length) {
                mergedArray[k++] = arr2[j++];
            }
            System.out.println(Arrays.toString(mergedArray));
        }
    }public static void mergeTwoArraysWithSystemArrayCopy(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, mergedArray, 0, arr1.length);
        System.arraycopy(arr2, 0, mergedArray, arr1.length, arr2.length);
        Arrays.sort(mergedArray);
        System.out.println(Arrays.toString(mergedArray));
    }public static void mergeTwoArraysWithLoop(int[] arr1, int[] arr2) {
        int[] mergedArray = new int[arr1.length + arr2.length];

        int pos = 0;
        //копируем элементы первого массива в наш результирующий
        for (int element : arr1) {
            mergedArray[pos] = element;
            pos++;
        }

        //копируем элементы второго массива в наш результирующий
        for (int element : arr2) {
            mergedArray[pos] = element;
            pos++;
        }

        //сортируем
        Arrays.sort(mergedArray);
        //выводим на экран
        System.out.println(Arrays.toString(mergedArray));
    }


    */


import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int n1 = input.nextInt();
        int[] arr1 = new int[n1];


        for (int i = 0; i < n1; i++) {
            arr1[i] = input.nextInt();
        }

        int n2 = input.nextInt();
        int[] arr2 = new int[n2];
        for (int i = 0; i < n2; i++) {
            arr2[i] = input.nextInt();
        }

        int n3 = n1 + n2;
        int[] arr3 = new int[n3];

        int i1=0, i2=0;
        for (int element : arr3) {
            if (arr1[i1] < arr2[i2]) {




            }
        }
    }
}
