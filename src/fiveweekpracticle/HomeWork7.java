package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        if (n > 0 && n < 100) {
            String[] names = new String[n];
            String[] owners = new String[n];
            float[] marks = new float[n];
            int[] winners = new int[]{-1, -1, -1};
            for (int i = 0; i < n; i++) {
                owners[i] = scanner.next();
            }
            for (int i = 0; i < n; i++) {
                names[i] = scanner.next();
            }
            for (int i = 0; i < n; i++) {
                float sum = 0.f;
                for (int j = 0; j < 3; j++) {
                    sum += scanner.nextFloat();
                }
                marks[i] = sum / 3;
                for (int j = 0; j < 3 && j <= i; j++) {
                    if (winners[j] == -1) {
                        winners[j] = i;
                        break;
                    }
                    if (marks[i] > marks[winners[j]]) {
                        for (int k = Math.min(2, i); k >= j + 1; k--) {
                            winners[k] = winners[k - 1];
                        }
                        winners[j] = i;
                        break;
                    }
                }
            }
            for (int i = 0; i < 3; i++) {
                System.out.printf("%s: %s, %.1f", owners[winners[i]], names[winners[i]], ((int) (marks[winners[i]] * 10)) / 10.);
                System.out.println();
            }

        }
    }
}