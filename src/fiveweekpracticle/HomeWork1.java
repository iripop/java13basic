package fiveweekpracticle;

import java.util.Scanner;

public class HomeWork1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        // что мы делаем символом ==
        if (n > 0 && n < 100) {

            int m = scanner.nextInt();
            if (m > 0 && m < 100) {
                int[][] a = new int[m][n];
                for (int j = 0; j < m; j++) {
                    for (int i = 0; i < n; i++) {
                        int ai = scanner.nextInt();
                        if (ai > 0 && ai < 1000) {
                            a[j][i] = ai;
                        }
                    }
                }
                printMin(a);
            }
        }
    }

    private static void printMin(int[][] a) {
        for (int j = 0; j < a.length; j++) {
            int min = a[j][0];
            for (int i = 1; i < a[j].length; i++) {
                if (a[j][i] < min) {
                    min = a[j][i];
                }
            }
            if (j > 0) {
                System.out.print(" ");
            }
            System.out.print(min);
        }
        System.out.println();
    }
}