package six;

import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int res= 1;
        for (int i = 1; i <= n; i++) {
            res *= i;

        }
        System.out.println(res);
        System.out.println(factorial(1, n));
    }

    public static int factorial(int res, int i) {
        if (i <= 0) {
            return res;
        }
        res *= i;
        return factorial(res, i - 1);
    }
}
