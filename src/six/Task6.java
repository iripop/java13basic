package six;
/*
На вход подается число N — ширина и высота матрицы.
Необходимо заполнить матрицу 1 и 0 в виде шахматной доски.
Нулевой элемент должен быть 0.

Входные данные
3
Выходные данные
0 1 0
1 0 1
0 1 0

Входные данные
4
Выходные данные
0 1 0 1
1 0 1 0
0 1 0 1
1 0 1 0
   */


import java.util.Arrays;
import java.util.Scanner;

public class Task6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int [][] a = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                a[i][j] = (j + i) % 2;

            }
            System.out.println(Arrays.toString(a[i]));

        }
    }

}
