package treeweekpractice;

import java.util.Scanner;

public class Home10 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            for (int k = 1; k<n-i; k++) {
                System.out.print(" ");
            }
            for (int j = 0; j < i*2 + 1; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
        //for ()

        for (int k = 1; k<n; k++) {
            System.out.print(" ");
        }
        System.out.println("|");
    }
}
