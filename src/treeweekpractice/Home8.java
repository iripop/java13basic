package treeweekpractice;

import java.util.Scanner;

public class Home8 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int p = scanner.nextInt();
        if (n > 0 && p > 0 && n < 1000 && p < 1000) {

            int[] arr = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = scanner.nextInt();
            }
            int sum = 0;

            for (int i = 0; i < arr.length; i++) {
                if (arr[i] > p) {
                    sum += arr[i];
                }
            }
            System.out.println(sum);
        }
    }
}

