package treeweekpractice;

import java.util.Scanner;

public class Home6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int noms[] = {8, 4, 2, 1};
        if (n > 0 && n < 1000000) {
            for (int nom = 0; nom < 4; nom++) {
                int k = n / noms[nom];
                n = n % noms[nom];
                System.out.println(k);
            }
        }
    }
}
